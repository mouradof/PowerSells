DOCKER_BUILD_SCRIPT := .bash/docker_build.sh
DOCKER_RUN_SCRIPT := .bash/docker_run.sh
DOCKER_STOP_SCRIPT := .bash/docker_stop.sh

.PHONY: help setup_build setup_run setup_stop docker_build docker_run docker_stop

# Colors
GREEN=\033[0;32m
CYAN=\033[0;36m
NC=\033[0m

help:
	@echo "${CYAN}Available targets:${NC}"
	@echo "  make ${GREEN}docker_build${NC}   Build Docker image & container & launch it"
	@echo "  make ${GREEN}docker_run${NC}     Run Docker container & launch it"
	@echo "  make ${GREEN}docker_stop${NC}    Stop Docker container"

setup_build:
	@chmod +x $(DOCKER_BUILD_SCRIPT)

setup_run:
	@chmod +x $(DOCKER_RUN_SCRIPT)

setup_stop:
	@chmod +x $(DOCKER_STOP_SCRIPT)

docker_build: setup_build
	@./$(DOCKER_BUILD_SCRIPT)

docker_run: setup_run
	@./$(DOCKER_RUN_SCRIPT)

docker_stop: setup_stop
	@./$(DOCKER_STOP_SCRIPT)
