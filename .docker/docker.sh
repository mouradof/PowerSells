until echo '\q' | mysql --password=$MYSQL_ROOT_PASSWORD --host=$MYSQL_HOST; do
    >&2 echo "MySQL is unavailable - sleeping"
    sleep 1
done

>&2 echo "MySQL IS UP!"

result=$(php bin/console doctrine:query:sql "SELECT COUNT(*) AS count FROM information_schema.tables WHERE table_schema = '${MYSQL_DATABASE}' AND table_name = 'user';")
count=$(echo "$result" | grep -o '[0-9]*' | cut -f2)

if [ $count -eq 0 ]; then
  echo "the database is empty"
  php bin/console doctrine:schema:update --force
    if [ -z $ADMIN_PASSWORD ]; then
    GENPASS=$(
    date +%s | sha256sum | base64 | head -c 12
    echo)
    export ADMIN_PASSWORD=$GENPASS
    echo "the password of user admin@localhost is ${ADMIN_PASSWORD}"
    fi
    php bin/console app:create-admin
else
  result=$(php bin/console doctrine:query:sql "SELECT COUNT(*) FROM mydb.user")
  count=$(echo "$result" | grep -o '[0-9]*' | cut -f2)
  if [ $count -eq 0 ]; then
  echo "there is no user; creating admin user"
  if [ -z $ADMIN_PASSWORD ]; then
  GENPASS=$(
  date +%s | sha256sum | base64 | head -c 12
  echo)
  export ADMIN_PASSWORD=$GENPASS
  echo "the password of user admin@localhost is ${ADMIN_PASSWORD}"
  fi
  php bin/console app:create-admin
  fi
fi

php bin/console doctrine:migrations:migrate --allow-no-migration
php bin/console cache:clear --env=prod

rm .env
echo DATABASE_URL=$DATABASE_URL > .env
echo APP_ENV=$APP_ENV >> .env
echo APP_SECRET=$APP_SECRET >> .env
echo MESSENGER_TRANSPORT_DSN=$MESSENGER_TRANSPORT_DSN >> .env
echo MAILER_DSN=$MAILER_DSN >> .env

rm -rf html && ln -s public html
echo "Application is ready to use"
exec apache2-foreground